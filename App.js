import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import ZaloPayExample from './ZaloPayExample'

const App = () => {
  return (
    <View>
      <Text>App</Text>
      <ZaloPayExample/>
    </View>
  )
}

export default App

const styles = StyleSheet.create({})